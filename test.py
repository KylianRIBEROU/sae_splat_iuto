# coding: utf-8
import argparse
import random
import client
import const
import plateau
import case
import joueur
import carac

def creer_le_calque(PLATEAU,POS):
    CALQUE = dict()
    for (POSITION,_) in PLATEAU.items():
        if type(POSITION) is tuple:
            CASE = plateau.get_case(PLATEAU,POSITION)
            CALQUE[POSITION] = (None if case.est_mur(CASE) else 0)
    IND = 1
    POSITION_A_BOUCLE = set()
    POSITION_A_BOUCLE.add(POS)
    while IND <= plateau.get_nb_lignes(PLATEAU)*plateau.get_nb_colonnes(PLATEAU):
        AJOUTER_POSITION = set()
        for POSITION in POSITION_A_BOUCLE:
            CALQUE[POSITION] = IND
            DIRECTION = plateau.directions_possibles(PLATEAU,POSITION).keys()
            for DIR in DIRECTION:
                DIRC = plateau.INC_DIRECTION[DIR]
                POS2 = ( (POSITION[0]+DIRC[0]),(POSITION[1]+DIRC[1]) )
                if CALQUE[POS2] == 0:
                    AJOUTER_POSITION.add(POS2)
        POSITION_A_BOUCLE = AJOUTER_POSITION
        IND+=1
    return CALQUE

def creer_le_chemin(PLATEAU,CALQUE,POS_DEPART,POS_ARRIVEE):
    CHEMIN = []
    CHEMIN.append(POS_DEPART)
    IND = CALQUE[POS_DEPART]
    POSITION_A_BOUCLE = set()
    POSITION_A_BOUCLE.add(POS_DEPART)
    while POSITION_A_BOUCLE != [] and IND > 0 and IND != CALQUE[POS_ARRIVEE]:
        AJOUTER_POSITION = set()
        for POSITION in POSITION_A_BOUCLE:
            DIRECTION = plateau.directions_possibles(PLATEAU,POSITION).keys()
            for DIR in DIRECTION:
                DIRC = plateau.INC_DIRECTION[DIR]
                POS2 = ( (POSITION[0]+DIRC[0]),(POSITION[1]+DIRC[1]) )
                if CALQUE[POS2] == IND-1:
                    CHEMIN.append(POS2)
                    AJOUTER_POSITION.add(POS2)
                    break
        POSITION_A_BOUCLE = AJOUTER_POSITION
        IND-=1
    return CHEMIN

def trouver_case_specifique_plus_pres(PLATEAU,DISTANCE_MAX,COULEUR,POS_DEPART):
    IND = 0
    POSITION_A_BOUCLE = set()
    POSITION_A_BOUCLE.add(POS_DEPART)
    while IND <= DISTANCE_MAX:
        AJOUTER_POSITION = set()
        for POSITION in POSITION_A_BOUCLE:
            DIRECTION = plateau.directions_possibles(PLATEAU,POSITION).keys()
            for DIR in DIRECTION:
                DIRC = plateau.INC_DIRECTION[DIR]
                POS2 = ( (POSITION[0]+DIRC[0]),(POSITION[1]+DIRC[1]) )
                if PLATEAU[POS2].upper() != COULEUR:
                    AJOUTER_POSITION.add(POS2)
                else:
                    return get_direction(POS_DEPART,POS2[0],POS2[1])
        POSITION_A_BOUCLE = AJOUTER_POSITION
        IND+=1
    return random.choice("NSEO")

def retourner_direction_tir(position_init, position_arrivee):
    # avec X les lignes et Y les colonnes
    # a combiner avec fonction directions_possibles pour éviter les bugs de sortie de map / sortie de couloir et limiter
    # les déplacements analysés a ceux de 1 case de distance SINON CA MARCHE
    (x1,y1)=position_init
    (x2,y2)=position_arrivee
    if position_init==position_arrivee:
        return 'X'
    if x2>x1 :
        return 'E'
    elif x2<x1:
        return 'O'
    elif y2>y1:
        return 'S'
    elif y2>y1:
        return 'N'
 

def get_direction(POSITION_JOUEUR,POS_X,POS_Y):
    return ("N" if POSITION_JOUEUR[0]-1 == POS_X else "S" 
    if POSITION_JOUEUR[0]+1 == POS_X else "O" 
     if POSITION_JOUEUR[1]-1 == POS_Y else "E" 
      if POSITION_JOUEUR[1]+1 == POS_Y else random.choice("NSEO"))

def get_position(POS_X,POS_Y,direction):
    if direction == "N":
        POS_X += 1
    elif direction == "S":
        POS_X -= 1
    if direction == "O":
        POS_Y += 1
    elif direction == "E":
        POS_Y -= 1
    return (POS_X,POS_Y)

def trouve_objet_proche(le_plateau, pos_joueur, objet_cherche):
    dico_pos_objets=dict()
    calque=creer_le_calque(le_plateau, pos_joueur)
    dico_pos_objets[objet_cherche]=[]
    for pos_objet in le_plateau['objets'][str(objet_cherche)]:
        dico_pos_objets[objet_cherche].append(creer_le_chemin(le_plateau, calque, pos_joueur, pos_objet))
    if dico_pos_objets[objet_cherche]==[]:
        return pos_joueur
    a=sorted(dico_pos_objets[objet_cherche], key=len)
    (x,y) = (a[0][-1] if a[0][-1] != pos_joueur else a[0][len(a[0])-2])
    return (x, y)

def mon_IA(ma_couleur,carac_jeu, plan, les_joueurs):
    """ Cette fonction permet de calculer les deux actions du joueur de couleur ma_couleur
        en fonction de l'état du jeu décrit par les paramètres. 
        Le premier caractère est parmi XSNOE X indique pas de peinture et les autres
        caractères indique la direction où peindre (Nord, Sud, Est ou Ouest)
        Le deuxième caractère est parmi SNOE indiquant la direction où se déplacer.

    Args:
        ma_couleur (str): un caractère en majuscule indiquant la couleur du jeur
        carac_jeu (str): une chaine de caractères contenant les caractéristiques
                                   de la partie séparées par des ;
             duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet           
        plan (str): le plan du plateau comme comme indiqué dans le sujet
        les_joueurs (str): le liste des joueurs avec leur caractéristique (1 joueur par ligne)
        couleur;reserve;nb_cases_peintes;objet;duree_objet;ligne;colonne;nom_complet
    
    Returns:
        str: une chaine de deux caractères en majuscules indiquant la direction de peinture
            et la direction de déplacement
    """
    # IA complètement aléatoire
    # a la fin faire des conditions selon les carac du jeu 
    # ici il faudra décoder le plan, les joueur et les caractéristiques du jeu
    return mon_IA_1(ma_couleur,carac_jeu,plan,les_joueurs)

# def liste_objets(le_plateau):
#     objets=dict()
#     plat=plateau.Plateau(le_plateau)
#     for position in plat['objets']:
#         if case.get_objet(position) not in objets:
#             objets[case.get_objet(position)]=set()
#         else:
#             objets[case.get_objet(position)].add(position)
#     return objets

# liste_objets(open(filedialog.askopenfilename(initialdir = os.getcwd(), title = "Choisir truc à charger", filetypes = (("txt files", "*.txt*"), ("all files","*.*")))).read())


def mon_IA_1(ma_couleur,carac_jeu, plan, les_joueurs):

    liste_autres_joueurs=[]
    le_plateau=plateau.Plateau(plan)
    mon_joueur = None
    for player in les_joueurs.split("\n"):
        PLAYER = joueur.joueur_from_str(player)
        if joueur.get_couleur(PLAYER)==ma_couleur: 
            mon_joueur = PLAYER
        else: 
            liste_autres_joueurs.append((joueur.get_couleur(PLAYER), joueur.get_reserve(PLAYER), joueur.get_surface(PLAYER), joueur.get_objet(PLAYER), joueur.get_pos(PLAYER)))

    reserve=joueur.get_reserve(mon_joueur)
    #calque=creer_le_calque()

    #############################################
    # Si réserve basse / négative
    #############################################
    if reserve <= 3:

        pos=joueur.get_pos(mon_joueur)
        if str(const.BIDON) in le_plateau['objets'].keys() and reserve<-10:
            dir=trouve_objet_proche(le_plateau, pos, const.BIDON)
            return 'X'+get_direction(pos,dir[0], dir[1])

            
# on vole peinture, jusqu'a 5

        for direction in plateau.directions_possibles(le_plateau, pos).keys():
            infos_action=plateau.peindre(le_plateau, pos, direction, ma_couleur, reserve, 5, False)
            if len(infos_action['joueurs_touches'])>1:
                tir=retourner_direction_tir(pos, get_position(pos[0],pos[1],direction))
                return tir+trouver_case_specifique_plus_pres(le_plateau, plateau.get_nb_colonnes(le_plateau),
                ma_couleur, pos)
            elif len(infos_action['joueurs_touches'])==1:
                for coul_joueur in infos_action['joueurs_touches']:
                    for liste in liste_autres_joueurs:
                        if coul_joueur in liste:
                            res=liste[1]
                            if infos_action['nb_repeintes']>=1 and res>=3:
                                tir=retourner_direction_tir(pos, get_position(pos[0],pos[1],direction))
                                return tir+trouver_case_specifique_plus_pres(le_plateau, plateau.get_nb_colonnes(le_plateau),
                                ma_couleur, pos)


        else:
    
            # si couleur pour se ravitailler en peinture a proximité
            return 'X'+trouver_case_specifique_plus_pres(le_plateau, plateau.get_nb_colonnes(le_plateau),
             ma_couleur, pos)

    #############################################
    # Sinon
    #############################################
    
    if const.PISTOLET in le_plateau["objets"].keys() and reserve>=10:
        dir=trouve_objet_proche(le_plateau, pos, const.PISTOLET)
        return 'X'+get_direction(pos,dir[0], dir[1]) # déplacement absolu jusqu'au pistolet : a changer
    
    return random.choice('XNOSE')+random.choice('NOSE')

    




if __name__=="__main__":
    parser = argparse.ArgumentParser()  
    parser.add_argument("--equipe", dest="nom_equipe", help="nom de l'équipe", type=str, default='Non fournie')
    parser.add_argument("--serveur", dest="serveur", help="serveur de jeu", type=str, default='localhost')
    parser.add_argument("--port", dest="port", help="port de connexion", type=int, default=1111)
    
    args = parser.parse_args()
    le_client=client.ClientCyber()
    le_client.creer_socket(args.serveur,args.port)
    le_client.enregistrement(args.nom_equipe,"joueur")
    ok=True
    while ok:
        ok,id_joueur,le_jeu=le_client.prochaine_commande()
        if ok:
            carac_jeu,le_plateau,les_joueurs=le_jeu.split("--------------------\n")
            actions_joueur=mon_IA(id_joueur,carac_jeu,le_plateau,les_joueurs[:-1])
            le_client.envoyer_commande_client(actions_joueur)
            # le_client.afficher_msg("sa reponse  envoyée "+str(id_joueur)+args.nom_equipe)
    le_client.afficher_msg("terminé")
