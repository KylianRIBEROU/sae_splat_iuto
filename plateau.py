"""module de gestion du plateau de jeu
"""
import const
import case

# dictionnaire permettant d'associer une direction et la position relative
# de la case qui se trouve dans cette direction
INC_DIRECTION = {'N': (-1, 0), 'E': (0, 1), 'S': (1, 0),
                 'O': (0, -1), 'X': (0, 0)}

def get_nb_lignes(plateau):
    """retourne le nombre de lignes du plateau

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: le nombre de lignes du plateau
    """
    return plateau["ligne"]

def get_nb_colonnes(plateau):
    """retourne le nombre de colonnes du plateau

    Args:
        plateau (dict): le plateau considéré

    Returns:
        int: le nombre de colonnes du plateau
    """
    return plateau["colonne"]

def get_case(plateau, pos):
    """retourne la case qui se trouve à la position pos du plateau

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        dict: La case qui se situe à la position pos du plateau
    """
    if (pos[0] < 0 or pos[0] >= get_nb_lignes(plateau)) or (pos[1] < 0 or pos[1] >= get_nb_colonnes(plateau)):
        return case.Case(mur=True)
    joueurs_presents = set()
    objet_present = const.AUCUN
    for key in plateau["joueurs"]:
        pos_joueur = plateau["joueurs"][key]
        if pos_joueur == pos:
            joueurs_presents.add(key)
    for key in plateau["objets"]:
        pos_objet = plateau["objets"][key]
        for POS_OBJET in pos_objet:
            if POS_OBJET == pos:
                objet_present = key
    couleur = plateau[pos]
    return case.Case(mur=couleur == "#" or couleur.islower(),couleur=couleur if couleur != "#" else " ",objet=int(objet_present),joueurs_presents=joueurs_presents if len(joueurs_presents) > 0 else None)      

def poser_joueur(plateau, joueur, pos):
    """pose un joueur en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        joueur (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int
    """
    plateau["joueurs"][joueur] = pos
    return None

def poser_objet(plateau, objet, pos):
    """Pose un objet en position pos sur le plateau. Si cette case contenait déjà
        un objet ce dernier disparait

    Args:
        plateau (dict): le plateau considéré
        objet (int): un entier représentant l'objet. const.AUCUN indique aucun objet
        pos (tuple): une paire (lig,col) de deux int
    """
    if plateau["objets"].get(objet) is None:
        POSITION = set()
        POSITION.add(pos)
        plateau["objets"][objet] = POSITION
    else:
        plateau["objets"][objet].add(pos)
    return None

def plateau_from_str(la_chaine):
    """Construit un plateau à partir d'une chaine de caractère contenant les informations
        sur le contenu du plateau (voir sujet)

    Args:
        la_chaine (str): la chaine de caractères décrivant le plateau

    Returns:
        dict: le plateau correspondant à la chaine. None si l'opération a échoué
    """
    ...

def Plateau(plan):
    """Créer un plateau en respectant le plan donné en paramètre.
        Le plan est une chaine de caractères contenant
            '#' (mur)
            ' ' (couloir non peint)
            une lettre majuscule (un couloir peint par le joueur représenté par la lettre)

    Args:
        plan (str): le plan sous la forme d'une chaine de caractères

    Returns:
        dict: Le plateau correspondant au plan
    """
    le_plateau = dict()
    ligne_plateau = plan.split("\n")
    le_plateau["ligne"] = int(ligne_plateau[0].split(";")[0])
    le_plateau["colonne"] = int(ligne_plateau[0].split(";")[1])
    le_plateau["joueurs"] = dict()
    le_plateau["objets"] = dict()
    ligne = 1
    while ligne < len(ligne_plateau):
        if ligne <= le_plateau["ligne"]:
            for colonne in range(len(ligne_plateau[ligne])):
                le_plateau[(ligne-1,colonne)] = ligne_plateau[ligne][colonne]
        else:
            joueur = ligne_plateau[ligne].split(";")
            if len(joueur) == 3:
                if not joueur[0].isdecimal():
                    le_plateau["joueurs"][joueur[0]] = (int(joueur[1]),int(joueur[2]))
                else:
                    if le_plateau["objets"].get(joueur[0]) is None:
                        POSITION = set()
                        POSITION.add(((int(joueur[1]),int(joueur[2]))))
                        le_plateau["objets"][joueur[0]] = POSITION
                    else:
                        le_plateau["objets"][joueur[0]].add(((int(joueur[1]),int(joueur[2]))))
        ligne+=1
    return le_plateau

def set_case(plateau, pos, une_case):
    """remplace la case qui se trouve en position pos du plateau par une_case

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de deux int
        une_case (dict): la nouvelle case
    """
    plateau[pos] = une_case["couleur"]
    for joueur_present in case.get_joueurs(une_case):
        plateau["joueurs"][joueur_present] = (pos[0],pos[1])
    plateau["objets"][case.get_objet(une_case)] = (pos[0],pos[1])
    return None

def enlever_joueur(plateau, joueur, pos):
    """enlève un joueur qui se trouve en position pos sur le plateau

    Args:
        plateau (dict): le plateau considéré
        joueur (str): la lettre représentant le joueur
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        bool: True si l'opération s'est bien déroulée, False sinon
    """
    case_pos = get_case(plateau,pos)
    est_sur_la_pos = case.prendre_joueur(case_pos,joueur)
    plateau["joueurs"][joueur] = (None,None)
    return est_sur_la_pos

def prendre_objet(plateau, pos):
    """Prend l'objet qui se trouve en position pos du plateau et retourne l'entier
        représentant cet objet. const.AUCUN indique qu'aucun objet se trouve sur case

    Args:
        plateau (dict): Le plateau considéré
        pos (tuple): une paire (lig,col) de deux int

    Returns:
        int: l'entier représentant l'objet qui se trouvait sur la case.
        const.AUCUN indique aucun objet
    """
    objet = case.prendre_objet(get_case(plateau,pos))
    if objet != const.AUCUN and plateau["objets"].get(str(objet)) is not None:
        plateau["objets"][str(objet)] = (None,None)
    return objet

def deplacer_joueur(plateau, joueur, pos, direction):
    """Déplace dans la direction indiquée un joueur se trouvant en position pos
        sur le plateau

    Args:
        plateau (dict): Le plateau considéré
        joueur (str): La lettre identifiant le joueur à déplacer
        pos (tuple): une paire (lig,col) d'int
        direction (str): une lettre parmie NSEO indiquant la direction du déplacement

    Returns:
        tuple: un tuple contenant 4 informations
            - un bool indiquant si le déplacement a pu se faire ou non
            - un int valeur une des 3 valeurs suivantes:
                *  1 la case d'arrivée est de la couleur du joueur
                *  0 la case d'arrivée n'est pas peinte
                * -1 la case d'arrivée est d'une couleur autre que celle du joueur
            - un int indiquant si un objet se trouvait sur la case d'arrivée (dans ce
                cas l'objet est pris de la case d'arrivée)
            - une paire (lig,col) indiquant la position d'arrivée du joueur (None si
                le joueur n'a pas pu se déplacer)
    """
    pos2 = ((pos[0]-1,pos[1]) if direction == "N" else (pos[0]+1,pos[1]) if direction == "S" else (pos[0],pos[1]+1) if direction == "E" else (pos[0],pos[1]-1) if direction == "O" else None)
    if pos2 is not None:
        case_pos = get_case(plateau,pos2)
        deplacement = not case.est_mur(case_pos) and plateau["joueurs"][joueur] == pos
        if deplacement:
            enlever_joueur(plateau,joueur,pos)
            poser_joueur(plateau,joueur,pos2)
        couleur = case.get_couleur(case_pos)
        prendre_objet(plateau,pos2)
    return (deplacement,(1 if couleur.upper() == joueur else 0 if couleur == " " else -1), case.get_objet(case_pos),pos2)

#-----------------------------
# fonctions d'observation du plateau
#-----------------------------

def surfaces_peintes(plateau, nb_joueurs):
    """retourne un dictionnaire indiquant le nombre de cases peintes pour chaque joueur.

    Args:
        plateau (dict): le plateau considéré
        nb_joueurs (int): le nombre de joueurs total participant à la partie

    Returns:
        dict: un dictionnaire dont les clées sont les identifiants joueurs et les
            valeurs le nombre de cases peintes par le joueur
    """
    surface = dict()
    for joueur in plateau["joueurs"]: 
        surface[joueur] = 0
    for ligne in range(get_nb_lignes(plateau)):
        for colonne in range(get_nb_colonnes(plateau)):
            couleur = case.get_couleur(get_case(plateau,(ligne,colonne)))
            if couleur is not None:
                if surface.get(couleur.upper()) is not None:
                    surface[couleur.upper()] += 1
    return surface

def directions_possibles(plateau,pos,murs=False):
    """ retourne les directions vers où il est possible de se déplacer à partir
        de la position pos

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): un couple d'entiers (ligne,colonne) indiquant la position de départ
    
    Returns:
        dict: un dictionnaire dont les clés sont les directions possibles et les valeurs la couleur
              de la case d'arrivée si on prend cette direction
              à partir de pos
    """
    direction_possible = dict()
    def couleur_et_direction(cases,direction):
        if murs or not case.est_mur(cases):
            direction_possible[direction] = case.get_couleur(cases)
    couleur_et_direction(get_case(plateau,(pos[0]-1,pos[1])),"N")
    couleur_et_direction(get_case(plateau,(pos[0]+1,pos[1])),"S")
    couleur_et_direction(get_case(plateau,(pos[0],pos[1]-1)),"O")
    couleur_et_direction(get_case(plateau,(pos[0],pos[1]+1)),"E")
    return direction_possible
    
def nb_joueurs_direction(plateau, pos, direction, distance_max):
    """indique combien de joueurs se trouve à portée sans protection de mur.
        Attention! il faut compter les joueurs qui sont sur la case pos

    Args:
        plateau (dict): le plateau considéré
        pos (_type_): la position à partir de laquelle on fait le recherche
        direction (str): un caractère 'N','O','S','E' indiquant dans quelle direction on regarde
    Returns:
        int: le nombre de joueurs à portée de peinture (ou qui risque de nous peindre)
    """
    joueurs = set()
    est_mur = False
    ind = 0
    while not est_mur and ind <= distance_max:
        pos2 = (pos[0]-ind,pos[1]) if direction == "N" else (pos[0]+ind,pos[1]) if direction == "S" else (pos[0],pos[1]-ind) if direction == "O" else (pos[0],pos[1]+ind)
        est_mur = case.est_mur(get_case(plateau,pos2))
        for joueur in plateau["joueurs"]:
            if plateau["joueurs"][joueur] == pos2:
                joueurs.add(joueur)
        ind+=1
    return len(joueurs)
    
def peindre(plateau, pos, direction, couleur, reserve, distance_max, peindre_murs=False):
    """ Peint avec la couleur les cases du plateau à partir de la position pos dans
        la direction indiquée en s'arrêtant au premier mur ou au bord du plateau ou
        lorsque que la distance maximum a été atteinte.

    Args:
        plateau (dict): le plateau considéré
        pos (tuple): une paire (lig,col) de int
        direction (str): un des caractères 'N','S','E','O' indiquant la direction de peinture
        couleur (str): une lettre indiquant l'idenfiant du joueur qui peint (couleur de la peinture)
        reserve (int): un entier indiquant la taille de la reserve de peinture du joueur
        distance_max (int): un entier indiquant la portée maximale du pistolet à peinture
        peindre_mur (bool): un booléen indiquant si on peint aussi les murs ou non

    Returns:
        dict: un dictionnaire avec 4 clés
                "cout": un entier indiquant le cout en unités de peinture de l'action
                "nb_repeintes": un entier indiquant le nombre de cases qui ont changé de couleur
                "nb_murs_repeints": un entier indiquant le nombre de murs qui ont changé de couleur
                "joueurs_touches": un ensemble (set) indiquant les joueurs touchés lors de l'action
    """
    joueurs_touches = set()
    cout = 0
    ind = 0
    nb_repeintes = 0
    nb_murs_repeints = 0
    while ind < distance_max:
        pos2 = (pos[0]-ind,pos[1]) if direction == "N" else (pos[0]+ind,pos[1]) if direction == "S" else (pos[0],pos[1]-ind) if direction == "O" else (pos[0],pos[1]+ind)
        case_pos = get_case(plateau,pos2)
        if case.est_mur(case_pos) and not peindre_murs:
            break
        if plateau.get(pos2) is not None:
            for joueur in plateau["joueurs"]:
                if plateau["joueurs"][joueur] == pos2:
                    joueurs_touches.add(joueur) 
            couleur_case = case.get_couleur(case_pos)
            calcul_cout = (1 if couleur_case == " " or couleur_case.upper() == couleur else 2 if couleur_case.upper() != couleur else 0)
            if (cout+calcul_cout) > reserve:
                break
            cout += calcul_cout
            plateau[pos2] = (couleur.lower() if peindre_murs and case.est_mur(case_pos) else couleur.upper() if couleur_case.upper() != couleur else plateau[pos2])
            nb_murs_repeints += (1 if peindre_murs and case.est_mur(case_pos) else 0)
            nb_repeintes += (1 if couleur_case.upper() != couleur else 0)
        ind+=1
    return {"cout":cout,"nb_repeintes":nb_repeintes,"nb_murs_repeints":nb_murs_repeints,"joueurs_touches":joueurs_touches}