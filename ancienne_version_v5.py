# coding: utf-8
import argparse
import random
import client
import const
import plateau
import case
import joueur
import carac

def get_direction(POSITION_JOUEUR,POS_X,POS_Y):
    """ Cette fonction renvois la direction d'une position par rapport à celle du joueur

    Args:
        POSITION_JOUEUR: un tuple (y,x) du joueur
        POS_X: position x (int) de la position utilisé
        POS_Y: position y (int) de la position utilisé
    Returns:
        str: une chaine de un caractères en majuscules indiquant la direction de déplacement
    """
    return ("N" if POSITION_JOUEUR[0]-1 == POS_X else "S" if POSITION_JOUEUR[0]+1 == POS_X else "O" if POSITION_JOUEUR[1]-1 == POS_Y else "E" if POSITION_JOUEUR[1]+1 == POS_Y else random.choice("NSEO"))

def mon_IA(ma_couleur,carac_jeu, plan, les_joueurs):
    """ Cette fonction permet de calculer les deux actions du joueur de couleur ma_couleur
        en fonction de l'état du jeu décrit par les paramètres. 
        Le premier caractère est parmi XSNOE X indique pas de peinture et les autres
        caractères indique la direction où peindre (Nord, Sud, Est ou Ouest)
        Le deuxième caractère est parmi SNOE indiquant la direction où se déplacer.

    Args:
        ma_couleur (str): un caractère en majuscule indiquant la couleur du jeur
        carac_jeu (str): une chaine de caractères contenant les caractéristiques
                                   de la partie séparées par des ;
             duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet           
        plan (str): le plan du plateau comme comme indiqué dans le sujet
        les_joueurs (str): le liste des joueurs avec leur caractéristique (1 joueur par ligne)
        couleur;reserve;nb_cases_peintes;objet;duree_objet;ligne;colonne;nom_complet
    
    Returns:
        str: une chaine de deux caractères en majuscules indiquant la direction de peinture
            et la direction de déplacement
    """
    # IA complètement aléatoire
    # ici il faudra décoder le plan, les joueur et les caractéristiques du jeu
    les_joueurs = les_joueurs.split("\n")
    player = None
    for PLAYER in les_joueurs:
        le_joueur = joueur.joueur_from_str(PLAYER)
        if ma_couleur == joueur.get_couleur(le_joueur):
            player = le_joueur
    if player is not None and joueur.get_couleur(player) == ma_couleur:

        plat = plateau.Plateau(plan)

        RESERVE = joueur.get_reserve(player)
        OBJET = joueur.get_objet(player)
        SURFACE = joueur.get_surface(player)
        COULEUR = joueur.get_couleur(player)
        POSITION = joueur.get_pos(player)

        list_positions = []

        position_case = trouver_meilleur_chemin_vers_case(plat,POSITION,COULEUR,True)
        position_objet = trouver_meilleur_chemin_vers_objet(plat,POSITION,const.AUCUN)
        position_ennemi = trouver_meilleur_chemin_vers_ennemi(plat,POSITION,les_joueurs,COULEUR)

        if position_case is not None:
            list_positions.append(position_case)
        if position_objet is not None:
            list_positions.append(position_objet)
        if position_ennemi is not None:
            list_positions.append(position_ennemi)

        position_choisi = None

        ########################################################
        # Tirer sur un mur ou sur un joueur avec le pistolet ? #
        ########################################################

        if list_positions != []:
            position_plus_proche = min(list_positions,key=len)
            if RESERVE > 2:
                if OBJET == const.BOUCLIER:
                    ennemi_proche = trouver_meilleur_chemin_vers_ennemi(plat,POSITION,les_joueurs,COULEUR)
                    position_choisi = (ennemi_proche if ennemi_proche is not None else position_choisi)
                    if position_choisi is None:
                        position_case = trouver_meilleur_chemin_vers_case(plat,POSITION,COULEUR,True)
                        position_choisi = (position_case if position_case is not None else position_choisi)
                if position_choisi is None:
                    caracteristique = carac.carac_from_str(carac_jeu)
                    if carac.get_duree_act(caracteristique) >= 150 and (len(les_joueurs) > 2 or OBJET != const.AUCUN):
                        ennemi_proche = trouver_meilleur_chemin_vers_ennemi(plat,POSITION,les_joueurs,ma_couleur)
                        position_choisi = (ennemi_proche if ennemi_proche is not None else position_choisi)
                    if position_choisi is None:
                        if position_objet is not None and len(position_objet) < 5:
                            position_choisi = (position_objet if position_objet is not None else position_choisi)
                        elif RESERVE >= 10 and (str(const.BOMBE) in plat["objets"].keys() and str(const.PISTOLET) in plat["objets"].keys()) and OBJET != const.PISTOLET and OBJET != const.BOMBE:
                            list_objets = []
                            pistolet_proche = trouver_meilleur_chemin_vers_objet(plat,POSITION,const.BOMBE)
                            bombe_proche = trouver_meilleur_chemin_vers_objet(plat,POSITION,const.PISTOLET)
                            if pistolet_proche is not None:
                                list_objets.append(pistolet_proche)
                            if bombe_proche is not None:
                                list_objets.append(bombe_proche)
                            position_choisi = (min(list_objets,key=len) if list_objets is not None else position_choisi)
                        elif RESERVE >= 10 and str(const.BOMBE) in plat["objets"].keys() and OBJET != const.PISTOLET and OBJET != const.BOMBE:
                            objet_proche = trouver_meilleur_chemin_vers_objet(plat,POSITION,const.BOMBE)
                            position_choisi = (objet_proche if objet_proche is not None else position_choisi)
                        elif RESERVE >= 10 and str(const.PISTOLET) in plat["objets"].keys() and OBJET != const.PISTOLET and OBJET != const.BOMBE:
                            objet_proche = trouver_meilleur_chemin_vers_objet(plat,POSITION,const.PISTOLET)
                            position_choisi = (objet_proche if objet_proche is not None else position_choisi)
            else:
                if str(const.BIDON) not in plat["objets"].keys() or RESERVE > -8:
                    case_proche = trouver_meilleur_chemin_vers_case(plat,POSITION,ma_couleur,False)
                    if case_proche is None:
                        case_proche = trouver_meilleur_chemin_vers_case(plat,POSITION,ma_couleur,True)
                    position_choisi = (case_proche if case_proche is not None else position_choisi)
                    if position_choisi is None:
                        objet_proche = trouver_meilleur_chemin_vers_objet(plat,POSITION,const.AUCUN)
                else:
                    objet_proche = trouver_meilleur_chemin_vers_objet(plat,POSITION,const.BIDON)
                    position_choisi = (objet_proche if objet_proche is not None else position_choisi)
            if OBJET == const.BOMBE:
                position_choisi = (position_case if position_case is not None else position_choisi)
            if position_choisi is None:
                position_choisi = (position_plus_proche if position_plus_proche is not None else position_choisi)

        if position_choisi is not None:

            if (RESERVE >= 20 or OBJET == const.BOUCLIER) and position_ennemi is not None:
                position_choisi = position_ennemi
            position_choisi = position_choisi[0]
            direction = get_direction(POSITION,position_choisi[0],position_choisi[1])
            tir = direction
            peindre = plateau.peindre(plat,POSITION,tir,ma_couleur,RESERVE,5,OBJET == const.PISTOLET)
            # FAIRE EN SORTE QUE SI C'EST PLUS DE CASE QUE DE MUR ALORS ALLER DANS CETTE DIRECTION
            if RESERVE > 0:
                if len(plateau.directions_possibles(plat,position_choisi,False).keys()) == 1 and position_choisi == position_case:
                    tir = get_direction(POSITION,position_choisi[0],position_choisi[1])
                    position_choisi = trouver_meilleur_chemin_vers_case(plat,POSITION,COULEUR,False)
                tir = get_direction_pour_objet(plat,tir,OBJET,POSITION,COULEUR,RESERVE)
            if peindre["nb_repeintes"] == 0:
                tir = "X"    
            return tir+direction

    return random.choice("XNSOE")+random.choice("NSEO")

def get_direction_pour_objet(plat,tir,objet,position,couleur,reserve):
    """ Cette fonction permet de choisir la direction à laquelle le joueur devrait tirer pour avoir le plus de
    case possible

    Args:
        plat: le plateau du plan
        tir: le prochain tir programmé par défaut
        objet: l'objet du joueur
        position: la position du joueur
        couleur: la couleur du joueur
        reserve: la réserve du joueur
    Returns:
        str: une chaine de un caractères en majuscules indiquant la direction de peinture
    """
    if objet == const.PISTOLET or objet == const.BOMBE:
        directions_murs=dict()
        for direction in plateau.directions_possibles(plat, position, True).keys():
            peindre=plateau.peindre(plat, position, direction, couleur, reserve, 5, objet == const.PISTOLET)
            directions_murs[direction] = (peindre['nb_repeintes'],peindre['nb_murs_repeints'],peindre['cout'],peindre['joueurs_touches'])
        if directions_murs != dict():
            def get_joueurs_touches(dirc):
                return directions_murs[dirc][3]
            direction_choisie=max(directions_murs,key=get_joueurs_touches)
            if directions_murs[direction_choisie][1] <= 1:
                def get_nb_mur(dirc):
                    return directions_murs[dirc][1]
                direction_choisie=max(directions_murs,key=get_nb_mur)
                if directions_murs[direction_choisie][1] == 0:
                    def get_nb_case(dirc):
                        return directions_murs[dirc][0]
                    direction_choisie=max(directions_murs,key=get_nb_case)
            if direction_choisie!='' and direction_choisie!=None:    
                return direction_choisie
    return tir

def trouver_le_meilleur_joueur(les_joueurs,critere="surface"):
    """ Cette fonction permet de trouver le meilleur joueur actuelle de la partie suivant un critère

    Args:
        les_joueurs: tous les joueurs de la partie
        critere: le critère qui va déterminer le meilleur
    Returns:
        list: une liste correspondant à toute les positions faisable du chemin (x,y)
    """
    les_joueurs_a_trier = []
    for PLAYER in les_joueurs:
        les_joueurs_a_trier.append(joueur.joueur_from_str(PLAYER))
    def get_critere(player):
        return player[critere]
    return (max(les_joueurs_a_trier,key=get_critere) if les_joueurs_a_trier != [] else None)


def trouver_meilleur_chemin_vers_ennemi(plat,position,les_joueurs,couleur):
    """ Cette fonction permet de trouver le chemin le plus court pour aller vers un autre joueur

    Args:
        plat: le plateau
        position: position à laquelle on part
        les_ennemis: les informations concernant tous les ennemis
    Returns:
        list: une liste correspondant à toute les positions faisable du chemin (x,y)
    """
    list_chemins_plus_court = []
    for ennemi in les_joueurs:
        ennemi = joueur.joueur_from_str(ennemi)
        if joueur.get_couleur(ennemi) != couleur:
            chemin = creer_le_chemin(plat,position,joueur.get_pos(ennemi))
            if chemin != []:
                list_chemins_plus_court.append(chemin)
    return (min(list_chemins_plus_court,key=len) if list_chemins_plus_court != [] else None)

def trouver_meilleur_chemin_vers_objet(plat,position,objet=const.AUCUN):
    """ Cette fonction permet de trouver le chemin le plus court pour aller vers un objet

    Args:
        plat: le plateau
        position: position à laquelle on part
        objet: objet que l'on cherche (Si objet = const.AUCUN alors on prendra le chemin le plus court)
    Returns:
        list: une liste correspondant à toute les positions faisable du chemin (x,y)
    """
    objets = plat["objets"]
    list_chemins_plus_court = []
    for type_objet,positions in objets.items():
        if objet == const.AUCUN or (objet != const.AUCUN and str(type_objet) == str(objet)):
            list_chemin_objet = []
            for position_objet in positions:
                chemin = creer_le_chemin(plat,position,position_objet)
                if chemin != []:
                    list_chemin_objet.append(chemin)
            if list_chemin_objet != []:
                list_chemins_plus_court.append(min(list_chemin_objet,key=len))
    return (min(list_chemins_plus_court,key=len) if list_chemins_plus_court != [] else None)

def trouver_meilleur_chemin_vers_case(plat,position,couleur,sauf_couleur=False):
    """ Cette fonction permet de trouver le chemin le plus court pour aller dans une case d'une certaine couleur

    Args:
        plat: le plateau
        position: position à laquelle on part
        couleur: couleur que l'on recherche
        sauf_couleur: True si on veut éviter la couleur marqué et False pour y aller
    Returns:
        list: une liste correspondant à toute les positions faisable du chemin (x,y)
    """
    historique_positions = set()
    positions_iterees = set()
    positions_iterees.add(position)
    ind = 0
    while len(positions_iterees) > 0 and ind < plateau.get_nb_colonnes(plat)*plateau.get_nb_lignes(plat):
        positions_finis = set()
        for POSITION in positions_iterees:
            couleur_case = case.get_couleur(plateau.get_case(plat,POSITION)).upper()
            if (not sauf_couleur and couleur_case == couleur) or (sauf_couleur and couleur_case != couleur):
                chemin = creer_le_chemin(plat,position,POSITION)
                if chemin != []:
                    return chemin
            for direction in plateau.directions_possibles(plat,POSITION,False):
                direction_from_position = plateau.INC_DIRECTION[direction]
                position_potentielle = ( (POSITION[0]+direction_from_position[0]),(POSITION[1]+direction_from_position[1]) )
                couleur_case = case.get_couleur(plateau.get_case(plat,position_potentielle)).upper()
                if (not sauf_couleur and couleur_case == couleur) or (sauf_couleur and couleur_case != couleur):
                    chemin = creer_le_chemin(plat,position,position_potentielle)
                    if chemin != []:
                        return chemin
                elif position_potentielle not in historique_positions:
                    positions_finis.add(position_potentielle)
                    historique_positions.add(position_potentielle)
        positions_iterees = positions_finis
        ind += 1
    return None

def creer_le_chemin(plat,position_depart,position_arrivee):
    """ Cette fonction permet de trouver le chemin le plus court par rapport à 2 positions

    Args:
        plat: le plateau
        position_depart: la position d'où on va commencer
        position_arrivee: la position où on va devoir aller
    
    Returns:
        list: une liste correspondant à toute les positions faisable du chemin (x,y)
    """
    calque = creer_le_calque(plat,position_arrivee)
    ind = calque[position_depart]
    chemin = []
    if position_depart == position_arrivee:
        chemin.append(position_arrivee)
    while ind > 0 or position_arrivee not in chemin and position_depart != position_arrivee:
        for direction in plateau.directions_possibles(plat,position_depart,False):
            direction_from_position = plateau.INC_DIRECTION[direction]
            position_potentielle = ( (position_depart[0]+direction_from_position[0]),(position_depart[1]+direction_from_position[1]) )
            if calque[position_potentielle] == ind-1:
                chemin.append(position_potentielle)
                position_depart = position_potentielle
                break
        ind-=1
    return chemin

def creer_le_calque(plat,position_depart):
    """ Cette fonction permet de créer le calque pour permettre de calculer le chemin

    Args:
        plat: le plateau
        position_depart: la position à laquelle on part
    
    Returns:
        dict: une dictionnaire avec l'ensemble des positions du plateau
        - Les murs sont à None et les couloirs sont numéro de 1 jusqu'à X
    """
    calque = dict()
    for x in range(plateau.get_nb_colonnes(plat)):
        for y in range(plateau.get_nb_lignes(plat)):
            calque[(y,x)] = (None if case.est_mur(plateau.get_case(plat,(y,x))) else 1 if (y,x) == position_depart else 0)
    ind = 2
    positions_iterees = set()
    positions_iterees.add(position_depart)
    while len(positions_iterees) > 0 and ind < plateau.get_nb_colonnes(plat)*plateau.get_nb_lignes(plat):
        positions_finis = set()
        for position in positions_iterees:
            for direction in plateau.directions_possibles(plat,position,False):
                direction_from_position = plateau.INC_DIRECTION[direction]
                position_potentielle = ( (position[0]+direction_from_position[0]),(position[1]+direction_from_position[1]) )
                if calque[position_potentielle] == 0:
                    calque[position_potentielle] = ind
                    positions_finis.add(position_potentielle)
        positions_iterees = positions_finis 
        ind+=1
    return calque

if __name__=="__main__":
    parser = argparse.ArgumentParser()  
    parser.add_argument("--equipe", dest="nom_equipe", help="nom de l'équipe", type=str, default='Non fournie')
    parser.add_argument("--serveur", dest="serveur", help="serveur de jeu", type=str, default='localhost')
    parser.add_argument("--port", dest="port", help="port de connexion", type=int, default=1111)
    
    args = parser.parse_args()
    le_client=client.ClientCyber()
    le_client.creer_socket(args.serveur,args.port)
    le_client.enregistrement(args.nom_equipe,"joueur")
    ok=True
    while ok:
        ok,id_joueur,le_jeu=le_client.prochaine_commande()
        if ok:
            carac_jeu,le_plateau,les_joueurs=le_jeu.split("--------------------\n")
            actions_joueur=mon_IA(id_joueur,carac_jeu,le_plateau,les_joueurs[:-1])
            le_client.envoyer_commande_client(actions_joueur)
            # le_client.afficher_msg("sa reponse  envoyée "+str(id_joueur)+args.nom_equipe)
    le_client.afficher_msg("terminé")
