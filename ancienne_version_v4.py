# coding: utf-8
import argparse
import random
import client
import const
import plateau
import case
import joueur
import carac

def get_direction(POSITION_JOUEUR,POS_X,POS_Y):
    """ Cette fonction renvois la direction d'une position par rapport à celle du joueur

    Args:
        POSITION_JOUEUR: un tuple (y,x) du joueur
        POS_X: position x (int) de la position utilisé
        POS_Y: position y (int) de la position utilisé
    Returns:
        str: une chaine de un caractères en majuscules indiquant la direction de déplacement
    """
    return ("N" if POSITION_JOUEUR[0]-1 == POS_X else "S" if POSITION_JOUEUR[0]+1 == POS_X else "O" if POSITION_JOUEUR[1]-1 == POS_Y else "E" if POSITION_JOUEUR[1]+1 == POS_Y else random.choice("NSEO"))

def mon_IA(ma_couleur,carac_jeu, plan, les_joueurs):
    """ Cette fonction permet de calculer les deux actions du joueur de couleur ma_couleur
        en fonction de l'état du jeu décrit par les paramètres. 
        Le premier caractère est parmi XSNOE X indique pas de peinture et les autres
        caractères indique la direction où peindre (Nord, Sud, Est ou Ouest)
        Le deuxième caractère est parmi SNOE indiquant la direction où se déplacer.

    Args:
        ma_couleur (str): un caractère en majuscule indiquant la couleur du jeur
        carac_jeu (str): une chaine de caractères contenant les caractéristiques
                                   de la partie séparées par des ;
             duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet           
        plan (str): le plan du plateau comme comme indiqué dans le sujet
        les_joueurs (str): le liste des joueurs avec leur caractéristique (1 joueur par ligne)
        couleur;reserve;nb_cases_peintes;objet;duree_objet;ligne;colonne;nom_complet
    
    Returns:
        str: une chaine de deux caractères en majuscules indiquant la direction de peinture
            et la direction de déplacement
    """
    # IA complètement aléatoire
    # ici il faudra décoder le plan, les joueur et les caractéristiques du jeu
    LES_JOUEURS = les_joueurs.split("\n")
    JOUEUR = joueur.joueur_from_str(LES_JOUEURS[0])
    LES_JOUEURS_ENNEMIS = []
    for PLAYER in LES_JOUEURS:
        LE_JOUEUR = joueur.joueur_from_str(PLAYER)
        if ma_couleur in PLAYER:
            JOUEUR = LE_JOUEUR
        else:
            LES_JOUEURS_ENNEMIS.append(LE_JOUEUR)
    if joueur.get_couleur(JOUEUR) == ma_couleur:

        POSITION_JOUEUR = joueur.get_pos(JOUEUR)
        RESERVE = joueur.get_reserve(JOUEUR)
        OBJET = joueur.get_objet(JOUEUR)
        SURFACE = joueur.get_surface(JOUEUR)

        PLATEAU = plateau.Plateau(plan)

        POSITION_OBJET,LONGUEUR_POS_OBJET = None,0
        POSITION_CASE,LONGUEUR_POS_CASE = None,0

        if RESERVE > 2:
            CARACTERE_DU_JEU = carac.carac_from_str(carac_jeu)
            if carac.get_duree_act(CARACTERE_DU_JEU) >= 150:
                def get_surface(PL):
                    return joueur.get_surface(PL)
                LE_MEILLEUR = max(LES_JOUEURS_ENNEMIS,key=get_surface)
                if joueur.get_surface(LE_MEILLEUR) > joueur.get_surface(JOUEUR):
                    POSITION_CASE,LONGUEUR_POS_CASE = trouver_case_specifique_plus_pres(PLATEAU,joueur.get_couleur(LE_MEILLEUR),POSITION_JOUEUR)
            if POSITION_CASE is None:
                if RESERVE >= 15 and str(const.BOMBE) in PLATEAU["objets"].keys() and (OBJET!=const.PISTOLET and OBJET!=const.BOMBE):
                    POSITION_OBJET,LONGUEUR_POS_OBJET = trouver_objet_plus_pres(PLATEAU,POSITION_JOUEUR,const.BOMBE,BLACKLIST=const.BOUCLIER)
                elif RESERVE >= 15 and str(const.PISTOLET) in PLATEAU["objets"].keys() and (OBJET!=const.PISTOLET and OBJET!=const.BOMBE):
                    POSITION_OBJET,LONGUEUR_POS_OBJET = trouver_objet_plus_pres(PLATEAU,POSITION_JOUEUR,const.PISTOLET,BLACKLIST=const.BOUCLIER)
                else:
                    POSITION_OBJET,LONGUEUR_POS_OBJET = trouver_objet_plus_pres(PLATEAU,POSITION_JOUEUR,BLACKLIST=const.BIDON) 
                    POSITION_CASE,LONGUEUR_POS_CASE = trouver_case_specifique_plus_pres(PLATEAU," ",POSITION_JOUEUR)
                    if POSITION_CASE is None:
                        POSITION_CASE,LONGUEUR_POS_CASE = trouver_case_specifique_plus_pres(PLATEAU,ma_couleur,POSITION_JOUEUR,None) 
        elif RESERVE <= 2:
            if str(const.BIDON) not in PLATEAU["objets"].keys() or RESERVE > -8:
                POSITION_CASE,LONGUEUR_POS_CASE = trouver_case_specifique_plus_pres(PLATEAU,ma_couleur,POSITION_JOUEUR)
                if POSITION_CASE is None:
                    POSITION_OBJET,LONGUEUR_POS_OBJET = trouver_objet_plus_pres(PLATEAU,POSITION_JOUEUR,BLACKLIST=const.BOUCLIER)
            else:
                POSITION_OBJET,LONGUEUR_POS_OBJET = trouver_objet_plus_pres(PLATEAU,POSITION_JOUEUR,const.BIDON,BLACKLIST=const.BOUCLIER)
        # Ne pas allez faire l'objet si il est trop loin par rapport aux ennemis
        PROCHAINE_POSITION = POSITION_OBJET
        if OBJET != const.AUCUN and (PROCHAINE_POSITION is None or (LONGUEUR_POS_OBJET <= 0 and LONGUEUR_POS_CASE > 0) or (LONGUEUR_POS_CASE > 0 and LONGUEUR_POS_CASE < LONGUEUR_POS_OBJET)):
            PROCHAINE_POSITION = POSITION_CASE

        if PROCHAINE_POSITION is None:
            PROCHAINE_POSITION = (POSITION_CASE if POSITION_CASE is not None else POSITION_OBJET)

        if PROCHAINE_POSITION is not None:
            PROCHAIN_TIR = "X"
            if RESERVE > 0:
                CASE = plateau.get_case(PLATEAU,PROCHAINE_POSITION)
                if case.get_couleur(CASE) != ma_couleur:
                    PROCHAIN_TIR = get_direction(POSITION_JOUEUR,PROCHAINE_POSITION[0],PROCHAINE_POSITION[1])
                if len(plateau.directions_possibles(PLATEAU,PROCHAINE_POSITION,False).keys()) == 1 and PROCHAINE_POSITION == POSITION_CASE and RESERVE > 2:
                    PROCHAIN_TIR = get_direction(POSITION_JOUEUR,PROCHAINE_POSITION[0],PROCHAINE_POSITION[1])
                    PROCHAINE_POSITION = trouver_case_specifique_plus_pres(PLATEAU,ma_couleur,POSITION_JOUEUR)
                PROCHAIN_TIR = get_direction_pour_objet(PROCHAIN_TIR,JOUEUR,POSITION_JOUEUR,ma_couleur,RESERVE,PLATEAU)
            return PROCHAIN_TIR+get_direction(POSITION_JOUEUR,PROCHAINE_POSITION[0],PROCHAINE_POSITION[1])
    return random.choice("XNSOE")+random.choice("NSEO")

def get_direction_pour_objet(PROCHAIN_TIR,JOUEUR,POSITION,COULEUR,RESERVE,PLATEAU):
    """ Cette fonction permet de choisir la direction à laquelle le joueur devrait tirer pour avoir le plus de
    case possible

    Args:
        PROCHAIN_TIR: le prochain tir programmé par défaut
        JOUEUR: le joueur concerné
        POSITION: la position du joueur
        COULEUR: la couleur du joueur
        RESERVE: la réserve du joueur
        PLATEAU: le plateau du plan
    Returns:
        str: une chaine de un caractères en majuscules indiquant la direction de peinture
    """
    DICO_DIR_MURS=dict()
    for DIRECTION in plateau.directions_possibles(PLATEAU, POSITION, True).keys():
        OU_PEINDRE=plateau.peindre(PLATEAU, POSITION, DIRECTION, COULEUR, RESERVE, 5, joueur.get_objet(JOUEUR)==const.PISTOLET)
        DICO_DIR_MURS[DIRECTION]=(OU_PEINDRE['nb_repeintes'], OU_PEINDRE['nb_murs_repeints'], OU_PEINDRE['cout'], OU_PEINDRE['joueurs_touches'])
    if DICO_DIR_MURS != dict():
        def getnbtouche(dirc):
            return DICO_DIR_MURS[dirc][3]
        DIR_CHOISIE=max(DICO_DIR_MURS,key=getnbtouche)    
        if DICO_DIR_MURS[DIR_CHOISIE][1] == 0:
            def get_nb_mur(dirc):
                return DICO_DIR_MURS[dirc][1]
            DIR_CHOISIE=max(DICO_DIR_MURS,key=get_nb_mur)
            if DICO_DIR_MURS[DIR_CHOISIE][1] == 0:
                def get_nb_case(dirc):
                    return DICO_DIR_MURS[dirc][0]
                DIR_CHOISIE=max(DICO_DIR_MURS,key=get_nb_case)
            if DIR_CHOISIE!='' and DIR_CHOISIE!=None:    
                return DIR_CHOISIE
    return PROCHAIN_TIR

def trouver_case_specifique_plus_pres(PLATEAU,MA_COULEUR,POS_DEPART,TOUTE_CASE=True):
    """ Cette fonction permet de trouver une case spécifique à une couleur sauf lorsque le paramètre 
    TOUTE_CASE est sur None, si cela est le cas alors il va retourner n'importe quelle position différente de
    la couleur.

    Args:
        PLATEAU: le plateau
        MA_COULEUR: la couleur du joueur
        POS_DEPART: la position à laquelle on part
        TOUTE_CASE: savoir si l'on veut toutes les cases de la couleur ou justement aucune de ses cases
    
    Returns:
        str: une chaine de un caractères en majuscules indiquant la direction de déplacement
    """
    IND = 0
    POSITION_A_BOUCLE = set()
    POSITION_A_BOUCLE.add(POS_DEPART)
    while IND <= plateau.get_nb_colonnes(PLATEAU)*plateau.get_nb_lignes(PLATEAU):
        AJOUTER_POSITION = set()
        for POSITION in POSITION_A_BOUCLE:
            DIRECTION = plateau.directions_possibles(PLATEAU,POSITION).keys()
            for DIR in DIRECTION:
                DIRC = plateau.INC_DIRECTION[DIR]
                POS2 = ( (POSITION[0]+DIRC[0]),(POSITION[1]+DIRC[1]) )
                if (TOUTE_CASE is not None and PLATEAU[POS2].upper() != MA_COULEUR) or (TOUTE_CASE is None and PLATEAU[POS2].upper() == MA_COULEUR):
                    AJOUTER_POSITION.add(POS2)
                elif POS2 != POS_DEPART:
                    CALQUE = creer_le_calque(PLATEAU,POS2)
                    CHEMIN = creer_le_chemin(PLATEAU,CALQUE,POS_DEPART,POS2)
                    if CHEMIN != []:
                        POSITION_CASE_PROCHE = CHEMIN[0]
                        return POSITION_CASE_PROCHE,len(CHEMIN)
        POSITION_A_BOUCLE = AJOUTER_POSITION
        IND+=1
    return None,-1

def trouver_objet_plus_pres(PLATEAU,POSITION_JOUEUR,OBJECT = None,BLACKLIST=const.AUCUN):
    """ Cette fonction permet de trouver l'objet le plus près du joueur

    Args:
        PLATEAU: le plateau
        POSITION_JOUEUR: la position du joueur
    
    Returns:
        str: une chaine de un caractères en majuscules indiquant la direction de déplacement
    """
    CHEMIN_LIST = []
    for OBJET_TYPE,OBJET_POSITION in PLATEAU["objets"].items():
        if (OBJECT is None or (OBJET_TYPE == str(OBJECT))) and BLACKLIST != OBJET_TYPE:
            CHEMIN = []
            for POSITION_OBJET in OBJET_POSITION:
                CALQUE = creer_le_calque(PLATEAU,POSITION_OBJET)
                CHEMIN = creer_le_chemin(PLATEAU,CALQUE,POSITION_JOUEUR,POSITION_OBJET)
                if POSITION_OBJET in CHEMIN:
                    CHEMIN_LIST.append(CHEMIN)
            if CHEMIN_LIST != []:
                CHEMIN = min(CHEMIN_LIST,key=len)
            if CHEMIN != []:
                POS_X,POS_Y = CHEMIN[0]
                return (POS_X,POS_Y),len(CHEMIN)
    return None,-1

def creer_le_calque(PLATEAU,POS):
    """ Cette fonction permet de créer le calque pour permettre de calculer le chemin

    Args:
        PLATEAU: le plateau
        POS: la position à laquelle on part
    
    Returns:
        dict: une dictionnaire avec l'ensemble des positions du plateau
        - Les murs sont à None et les couloirs sont numéro de 1 jusqu'à X
    """
    CALQUE = dict()
    for (POSITION,_) in PLATEAU.items():
        if type(POSITION) is tuple:
            CASE = plateau.get_case(PLATEAU,POSITION)
            CALQUE[POSITION] = (None if case.est_mur(CASE) else 0)
    IND = 1
    POSITION_A_BOUCLE = set()
    POSITION_A_BOUCLE.add(POS)
    while IND <= plateau.get_nb_lignes(PLATEAU)*plateau.get_nb_colonnes(PLATEAU):
        AJOUTER_POSITION = set()
        for POSITION in POSITION_A_BOUCLE:
            CALQUE[POSITION] = IND
            DIRECTION = plateau.directions_possibles(PLATEAU,POSITION).keys()
            for DIR in DIRECTION:
                DIRC = plateau.INC_DIRECTION[DIR]
                POS2 = ( (POSITION[0]+DIRC[0]),(POSITION[1]+DIRC[1]) )
                if CALQUE[POS2] == 0:
                    AJOUTER_POSITION.add(POS2)
        POSITION_A_BOUCLE = AJOUTER_POSITION
        IND+=1
    return CALQUE

def creer_le_chemin(PLATEAU,CALQUE,POS_DEPART,POS_ARRIVEE):
    """ Cette fonction permet de trouver le chemin le plus court par rapport à 2 positions

    Args:
        PLATEAU: le plateau
        CALQUE: le calque du plateau
        POS_DEPART: la position d'où on va commencer
        POS_ARRIVEE: la position où on va devoir aller
    
    Returns:
        list: une liste correspondant à toute les positions faisable du chemin (x,y)
    """
    CHEMIN = []
    IND = CALQUE[POS_DEPART]
    POSITION_A_BOUCLE = set()
    POSITION_A_BOUCLE.add(POS_DEPART)
    while POSITION_A_BOUCLE != [] and IND > 0 and IND != CALQUE[POS_ARRIVEE]-1:
        AJOUTER_POSITION = set()
        for POSITION in POSITION_A_BOUCLE:
            DIRECTION = plateau.directions_possibles(PLATEAU,POSITION).keys()
            for DIR in DIRECTION:
                DIRC = plateau.INC_DIRECTION[DIR]
                POS2 = ( (POSITION[0]+DIRC[0]),(POSITION[1]+DIRC[1]) )
                if CALQUE[POS2] == IND-1 and CALQUE[POS2] is not None:
                    CHEMIN.append(POS2)
                    AJOUTER_POSITION.add(POS2)
                    break
        POSITION_A_BOUCLE = AJOUTER_POSITION
        IND-=1
    return CHEMIN

if __name__=="__main__":
    parser = argparse.ArgumentParser()  
    parser.add_argument("--equipe", dest="nom_equipe", help="nom de l'équipe", type=str, default='Non fournie')
    parser.add_argument("--serveur", dest="serveur", help="serveur de jeu", type=str, default='localhost')
    parser.add_argument("--port", dest="port", help="port de connexion", type=int, default=1111)
    
    args = parser.parse_args()
    le_client=client.ClientCyber()
    le_client.creer_socket(args.serveur,args.port)
    le_client.enregistrement(args.nom_equipe,"joueur")
    ok=True
    while ok:
        ok,id_joueur,le_jeu=le_client.prochaine_commande()
        if ok:
            carac_jeu,le_plateau,les_joueurs=le_jeu.split("--------------------\n")
            actions_joueur=mon_IA(id_joueur,carac_jeu,le_plateau,les_joueurs[:-1])
            le_client.envoyer_commande_client(actions_joueur)
            # le_client.afficher_msg("sa reponse  envoyée "+str(id_joueur)+args.nom_equipe)
    le_client.afficher_msg("terminé")
