

def carac_from_str(carac):
    """créer une liste de caractéristiques à partir d'un chaine de caractères qui contient
        ses caractéristiques séparées par des ; dans l'ordre suivant:
        "duree_act;duree_tot;reserve_init;duree_obj;penalite;bonus_touche;bonus_rechar;bonus_objet" 

    Args:
        description (str): la chaine de caractères contenant les caractéristiques
                            du jeu

    Returns:
        dict: les caractéristiques de la chaine.
    """
    liste_sep=carac.split(';')
    dico_carac_jeu={"duree_act":int(liste_sep[0]), "duree_tot":int(liste_sep[1]), 'reserve_init': int(liste_sep[2]), 'duree_objet': int(liste_sep[3]), 'penalite': int(liste_sep[4]), 'bonus_touche':int(liste_sep[5]), 'bonus_rechar':int(liste_sep[6]), 'bonus_objet':int(liste_sep[7])}
    return dico_carac_jeu

def get_duree_act(dico_carac):
    return dico_carac['duree_act']

def get_duree_tot(dico_carac):
    return dico_carac['duree_tot']

def get_reserve_init(dico_carac):
    return dico_carac['reserve_init']

def get_duree_objet_init(dico_carac):
    return dico_carac['duree_objet']

def get_penalite(dico_carac):
    return dico_carac['penalite']

def get_bonus_touche(dico_carac):
    return dico_carac['bonus_touche']

def get_bonus_rechar(dico_carac):
    return dico_carac["bonus_rechar"]
    
def get_bonus_objet(dico_carac):
    return dico_carac['bonus_objet']